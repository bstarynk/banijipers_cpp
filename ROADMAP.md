<!-- file ROADMAP.md of Banijipers++ -->
# Roadmap

Define naming conventions.

Decide on build automation tool (probably GNU make, perhaps with Guile extensions or plugins)

Incorporate the Ravenbrook MPS garbage collector.

Code a mostly C header file for immutable values (allocated using MPS,
and its [allocation point
protocol](https://www.ravenbrook.com/project/mps/master/manual/html/topic/allocation.html#topic-allocation-point-protocol),
with small inline functions to allocate and build (exactly like in
Bismon) the following kind of immutable values:

* boxed floating point
* boxed UTF-8 string
* boxed tuple (of object references)
* boxed set (of object references)

Some *ideas* from aborted
[meltmoni.hh](https://github.com/bstarynk/melt-monitor/blob/master/meltmoni.hh)
might be inspirational, if adapted to MPS.
