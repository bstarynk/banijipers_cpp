# Banijipers++ (a.k.a. banijipers_cpp)

Most of the goals and ideas of [banijipers-rust](https://gitlab.com/bstarynk/banijipers_rust) apply here. 

And many but not all ideas of
[bismon](http://github.com/bstarynk/bismon) also *indirectly*
apply. Basile Starynkevitch would describe that Banijipers++ as
"Bismon done right", at least as a persistent reflexive system (but
not as a static source code analysis framework).

It is the second tentative of the same project, but here we are trying to code in C++, the previous tentative was in Rust.

Static source code analysis is not viewed as an application of *Banijipers++*.

We want to use both [Ravenbrook MPS](https://www.ravenbrook.com/project/mps) and
[libgccjit](https://gcc.gnu.org/onlinedocs/jit/), both in their latest
released but stable versions.

## Authors

Niklas Rozencrantz (Sweden) : [niklasro@gmail.com](mailto:niklasro@gmail.com)
home page: [www.csc.kth.se](http://www.csc.kth.se/~nik/)

Basile Starynkevitch (France) : [basile@starynkevitch.net](mailto:basile@starynkevitch.net)
home page: [starynkevitch.net/Basile/](http://starynkevitch.net/Basile/index_en.html)

